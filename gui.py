#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
This program reads the Statistics data (distance, avareage speed, time, etc.) from an GPS track, supplied in GPX files,
and displays the found data in a Treeview.
Specifically GPX files (which are based on XML) written by Garmin GPS devices, which contain TrackStatsExtensions.
"""

__author__ = 'jos'
__version__ = "1.0"

import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

import gpx

class Mainwindow(Gtk.Window):
    """
    This class displays a gui window with a button to open a GPX file and some labels and a treeview to display
    Information from it.
    """

    def __init__(self):
        """
        Constructor for our main window
        """

        Gtk.Window.__init__(self, title="TrackStats", )
        self.set_border_width(5)

        # Use grid layout
        grid = Gtk.Grid()
        self.add(grid)
        grid.set_row_spacing(10)
        grid.set_column_spacing(10)

        # Add a file open button
        button_open = Gtk.Button(label="Datei öffnen")
        button_open.connect("clicked", self.on_button_open_clicked)
        grid.attach(button_open, 0, 0, 1, 1)

        # Add some labels for filename and track metadata
        # The spaces help give a good column width when no file is loaded yet
        self.labelFileName = Gtk.Label("                        ")
        grid.attach(self.labelFileName, 1, 0, 1, 1)
        label_caption_name = Gtk.Label("Track Name")
        grid.attach(label_caption_name, 0, 1, 1, 1)
        label_caption_time = Gtk.Label("Zeitstempel")
        grid.attach(label_caption_time, 0, 2, 1, 1)
        self.labelName = Gtk.Label()
        grid.attach(self.labelName, 1, 1, 1, 1)
        self.labelTime = Gtk.Label()
        grid.attach(self.labelTime, 1, 2, 1, 1)

        # Create a ListStore and a TreeView. The store will later be populated by contents from loaded file
        self.store = Gtk.ListStore(str, str)
        tree = Gtk.TreeView(self.store)
        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Tag", renderer, text=0)
        column.set_sort_column_id(0)
        tree.append_column(column)
        column = Gtk.TreeViewColumn("text", renderer, text=1)
        column.set_sort_column_id(1)
        tree.append_column(column)
        grid.attach(tree, 0, 3,2, 1)

    def on_button_open_clicked(self, widget):
        """
        Shows a file choser for GPX files and loads teh GPX file by using gpx.py. Populates the ListStore from
        the loaded data.
        :param widget:
        :return:
        """
        dialog = Gtk.FileChooserDialog("Bitte Datei wählen", self,
                                       Gtk.FileChooserAction.OPEN,
                                       (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                                        Gtk.STOCK_OPEN, Gtk.ResponseType.OK))

        file_filter = Gtk.FileFilter()
        file_filter.set_name("GPX Dateien")
        file_filter.add_pattern("*.[Gg][Pp][Xx]")
        dialog.add_filter(file_filter)

        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            file = dialog.get_filename()

        dialog.destroy()

        if response == Gtk.ResponseType.CANCEL:
            return

        # Parse the selected GPX file. If the file does not contain the tags we look for,
        # an Exception will be raise, which we cathc here (to show a message and return)
        try:
            gpx_data = gpx.GpxData(file)
        except Exception as ex:
            dialog = Gtk.MessageDialog(self, 0, Gtk.MessageType.ERROR,
                                       Gtk.ButtonsType.CLOSE, "Fehler beim lesen der Datei:")
            dialog.format_secondary_text(ex.args[0])
            dialog.run()
            dialog.destroy()
            return

        # If we reach here, it seems that an GPX file was successfully parsed and the searched
        # tags where found.

        # Strip the path from the file name and display it
        if '/' in file:
            file = file.split('/')[-1]
        elif '\\' in file:
            file = file.split('\\')[-1]
        self.labelFileName.set_text(file)

        # Display track name and timestamp
        self.labelName.set_text(gpx_data.get_metadata()["name"])
        self.labelTime.set_text(gpx_data.get_metadata()["time"])

        # Clear the store (needed in case we have read another file before and populate it with new values
        self.store.clear()
        for t, c in gpx_data.get_stats().items():
            self.store.append((t, c))

        self.store.set_sort_column_id(0, Gtk.SortType.ASCENDING)


if __name__ == "__main__":
    mainWindow = Mainwindow()
    mainWindow.connect("destroy", Gtk.main_quit)
    mainWindow.show_all()
    Gtk.main()
