# -*- encoding: utf-8 -*-

__author__ = "jos"

import xml.etree.ElementTree as ET


class GpxData(object):

    namespace = { "gpx" : "http://www.topografix.com/GPX/1/1",
                  "stats" : "http://www.garmin.com/xmlschemas/TrackStatsExtension/v1"}

    def __init__(self, file_name):
        tree = ET.parse(file_name)
        root = tree.getroot()

        # Search for name and time of the GPX track
        time = self.find_recursive(root, "gpx:time", GpxData.namespace)
        name = self.find_recursive(root, "gpx:name", GpxData.namespace)

        # Strip the namespace from the tag
        if '}' in time.tag:
            time.tag =  time.tag.split('}', 1)[1]
        if '}' in name.tag:
            name.tag =  name.tag.split('}', 1)[1]

        self.__metaData = {}
        self.__metaData [time.tag] = time.text
        self.__metaData [name.tag] = name.text

        # Find all childs of TrackStatsExtension, as these hold the information we are looking for
        self.__stats = {}
        extension = self.find_recursive(root, "stats:TrackStatsExtension", GpxData.namespace)

        # If no TrackStatsExtension found in file, raise exception
        if extension == None:
            raise Exception("Die GPX Datei enthält keine Garmin TrackStatsExtension,\n"
                            "also genau die gesuchten Infos.")

        for child in extension:
            # Again we don't want to have the namespace in the tags
            if '}' in child.tag:
                child.tag =  child.tag.split('}', 1)[1]
            self.__stats[child.tag] = child.text

    def get_metadata(self):
        return self.__metaData

    def get_stats(self):
        return self.__stats

    def find_recursive (self, element, tag, namespace):
        """
        This function first checks if the searched tag is a direct child and if
        not walks over the child nodes recursively
        :param element: The root element for the search (not necessarily the tree root)
        :param tag: The tag of the element to serach for
        :param namespace: optional namespace
        :return: the found element or None
        """
        # check if the searched element is a direct child
        result = element.find(tag, namespace)
        if isinstance(result, ET.Element):
            return result

        for child in element:
            result = self.find_recursive(child, tag, namespace)
            if isinstance(result, ET.Element):
                return result

        return None


